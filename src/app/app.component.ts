import { Component, Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';


interface Lang {
  value: string;
  viewValue: string;
}


interface Translation {
  targetText: string;
  from: string;
  to: string;
  translatedText: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'translate';

  langs: Lang[] = [
    {value:"ar",viewValue:"Arabic"},
    {value:"en",viewValue:"English"},
    {value:"es",viewValue:"Spanish"}
  ]

  displayedColumns = ["TargetText","from", "to","translatedText"]
  translations: Translation[] = []

  @Input() originalText = "text";
  @Input() selectedFrom = "en";
  @Input() selectedTo = "ar";
  @Input() translation = "transalted"


  checkoutForm = this.formBuilder.group({
    targetText: '',
    from: '',
    to: '',
    translatedText: ''
  });

  
  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient

  ) {
   }

  onSubmit(): void {
    // Process checkout data here
     this.http.post<Translation>("http://127.0.0.1:8080/translate",this.checkoutForm.value).subscribe(
       (resp)=>{
        this.translations.push(resp)
       }
     );
    console.warn('Your order has been submitted', this.checkoutForm.value);
    this.checkoutForm.reset()

  }


}
